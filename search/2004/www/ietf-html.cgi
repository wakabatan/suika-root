#!/usr/bin/perl
use strict;

my $uri = $ENV{PATH_INFO};
$uri =~ s!^/!!;
$uri =~ s/%([0-9A-F][0-9A-F])/pack 'C', hex $1/ge;
$uri =~ s/[^\x21-\x7E]//g;
print <<EOH;
Status: 301 See Other
Location: http://listserv.heanet.ie/cgi-bin/wa?$uri

EOH

