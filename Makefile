all: build

build: list.ja.html.u8 list.en.html.u8

CURL = curl
HARUSAME = ./.harusame

$(HARUSAME):
	$(CURL) -sSLf https://raw.githubusercontent.com/wakaba/harusame/master/harusame > $@
	chmod u+x $@

%.en.html.u8: %-src.html.u8 $(HARUSAME)
	$(HARUSAME) --lang en < $< > $@

%.ja.html.u8: %-src.html.u8 $(HARUSAME)
	$(HARUSAME) --lang ja < $< > $@

## License: Public Domain.
