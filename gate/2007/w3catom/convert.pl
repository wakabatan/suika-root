#!/usr/bin/perl
use strict;

use lib qw[/home/httpd/html/www/markup/html/whatpm
           /home/wakaba/work/manakai2/lib];

my @local_part = qw(
  ietf-dav-versioning
  ietf-http-wg
  public-backplane-comments
  public-canvas-api
  public-cdf
  public-diselect-editors
  public-forms
  public-forms-tf
  public-geolocation
  public-html
  public-html-a11y
  public-html-bugzilla
  public-html-comments
  public-html-commits
  public-html-diffs
  public-html-mail
  public-html-testsuite
  public-html-wg-announce
  public-html-wg-issue-tracking
  public-ietf-w3c
  public-media-annotation
  public-media-fragment
  public-n3-discuss
  public-new-work
  public-new-work-comments
  public-pfwg-comments
  public-rdfa
  public-script-coord
  public-svg-ig
  public-svg-ig-jp
  public-svg-implementors
  public-svg-media-type
  public-svg-print
  public-svg-wg
  public-uwa
  public-video-comments
  public-web-http-desc
  public-webapps
  public-webcgm
  public-webcgm-wg
  public-web-perf
  public-xg-app-backplane
  public-xg-geo
  public-xg-mmsem
  public-xhtml2
  public-xml-core-wg
  public-xml-id
  public-xml-testsuite
  public-xpointer-registry
  w3c-translators
  www-forms-editor
  www-xml-blueberry-comments
  www-xml-infoset-comments
  www-xml-linking-comments
  www-xml-stylesheet-comments
  www-xpath-comments
  xml-editor
  xml-names-editor
);

## Inactive lists:
#  public-xforms

# http-state
# es-discuss
# html4all

chdir '/home/httpd/html/gate/2007/w3catom';

use Encode;

use Message::DOM::DOMImplementation;
my $impl = Message::DOM::DOMImplementation->new;

my $doc = $impl->create_document;
$doc->manakai_is_html (1);

for my $local_part (@local_part) {
  warn $local_part, "...\n";
  
  my $index_uri = qq<http://lists.w3.org/Archives/Public/$local_part/>;
  my $index_file_name = '.index.html';
  $doc->document_uri ($index_uri);
  system 'wget', '-O', $index_file_name, $index_uri;
  die unless -s $index_file_name;
  
  {
    open my $index_file, '<', $index_file_name
        or die "$0: $index_file_name: $!";
    local $/ = undef;
    $doc->inner_html (Encode::decode ('utf8', scalar <$index_file>));
  }
  
  my @thread_uri;
  for my $a_el (@{$doc->query_selector_all
                    ('a:-manakai-contains("by thread")')}) {
    push @thread_uri, $a_el->href;
    last if @thread_uri == 2;
  }
  my $title = $local_part.'@w3.org';
  my $id = q<tag:suika.fam.cx,2007-10:>.$local_part;
  my $lang = 'en';
  
  my $feed_doc = $impl->create_atom_feed_document
      ($id, $title, $lang);
  $feed_doc->dom_config->set_parameter
      (q<http://suika.fam.cx/www/2006/dom-config/create-child-element> => 1);
  my $feed = $feed_doc->document_element;
  
  for ($feed->append_child ($feed_doc->create_element_ns
                                ($feed->namespace_uri, 'link'))) {
    $_->rel ('self');
    $_->href (qq<http://suika.fam.cx/gate/2007/w3catom/$local_part>);
    $_->type ('application/atom+xml');
  }
  
  for my $thread_uri (@thread_uri) {
    my $thread_file_name = '.thread.html';
    system 'wget', '-O', $thread_file_name, $thread_uri;
    die unless -s $thread_file_name;
    my $year = 0;
    $year = $1 if $thread_uri =~ /(\d{4,})/;
    
    $doc->document_uri ($thread_uri);
    
    {
      open my $thread_file, '<', $thread_file_name
          or die "$0: $thread_file_name: $!";
      local $/ = undef;
      $doc->inner_html (Encode::decode ('utf8', scalar <$thread_file>));
    }
    
    my $ul_el = $doc->query_selector ('body > div + div > ul');
    for my $li_el (@{$ul_el->query_selector_all ('li')}) {
      my $a_el = $li_el->query_selector (':-manakai-current > a[href]');
      my $uri = my $entry_id = $a_el->href;
      
      my $entry = $feed->add_new_entry ($entry_id, $a_el->text_content);
      
      for ($entry->append_child ($feed_doc->create_element_ns 
                                     ($entry->namespace_uri, 'link'))) {
        $_->rel ('alternate');
        $_->href ($uri);
        $_->type ('text/html');
      }
      
      for ($entry->append_child ($feed_doc->create_element_ns
                                     ($entry->namespace_uri, 'author'))) {
        $_->name ($li_el->query_selector (':-manakai-current > a[name]')
                      ->text_content);
      }
      
      if ($li_el->query_selector (':-manakai-current > em')->text_content
              =~ /(\d+)\s*(\w+)/) {
        $entry->published_element->text_content
            (my $val = sprintf '%04d-%02d-%02dT00:00:00-00:00',
             $year,
             {qw/January 1 February 2 March 3 April 4 May 5 June 6
                 July 7 August 8 September 9 October 10 November 11
                 December 12/}->{$2},
             $1);
        $entry->updated_element->text_content ($val);
      }
    }
  }
  
  my $feed_file_name = "$local_part.en.atom";
  open my $feed_file, '>:utf8', $feed_file_name
      or die "$0: $feed_file_name: $!";
  print $feed_file $feed_doc->inner_html;
}

